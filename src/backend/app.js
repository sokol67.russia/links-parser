import axios from "axios"
import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

const port = 3001
const app = express()
const requests = [
  "https://storage.yandexcloud.net/ccscenario/00e55ba2bc529823c73edca5935b2366.json",
  "https://storage.yandexcloud.net/ccscenario/0497128d3f276247bdd751ba34cf5d80.json",
  "https://storage.yandexcloud.net/ccscenario/0b8ab09975fbd4589879351a6a51021d.json"
]

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

app.post('/', async (req, res) => {
  const links = req.body.links
  const result = req.query.isParallel === 'true' ? await getLinksAll(links) : await getLinksByOne(links)

  try {
    res.status(200).send({ result })
  } catch (e) {
    res.status(500).send('Ошибка: ' + e.message)
  }
})

async function getLinksByOne (links = requests) {
  const result = []
  for (const link of links) result.push(await parseLink(link))

  return result
}

async function getLinksAll(links = requests) {
  return Promise.all(links.map(parseLink))
}

async function parseLink (link) {
  const { data } = await axios.get(link) //TODO::обработка ошибок
  return data
}

app.listen(port, async () => {
  const message = `Приложение "Links Parser" запущено, порт - ${port}`
  console.log(message)
  try {
  } catch {}
})
