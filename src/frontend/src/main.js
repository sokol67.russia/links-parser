import Vue from 'vue'
import App from './App.vue'
import AntDesign from 'ant-design-vue'

Vue.config.productionTip = false
Vue.use(AntDesign)

new Vue({
  render: h => h(App),
}).$mount('#app')
